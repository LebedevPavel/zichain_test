module.exports = {
	handleSuccess(res, data, message) {
		res.json({
			data,
			message,
			error: false
		})
	},
	handleError(res, error, message) {
		res.status(500).json({
			message: message || 'Произошла ошибка',
			tech_message: error.message || 'Unknown error',
			error: true
		})
	}
}