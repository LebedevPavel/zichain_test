const fs = require('fs')
const express = require('express')

const app = express()
app.use(express.json())

const routes = fs.readdirSync('routes');

routes.forEach(routeName => {
    const {path, router} = require(`./routes/${routeName}`);
    app.use(path, router);
    console.log(`Route added: ${path}`)
});

const port = process.env.PORT || 3000

app.listen(port, () => {
    console.log(`Server started on port ${port}`)
})