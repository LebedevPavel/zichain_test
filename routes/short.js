const express = require('express')
const router = express.Router()

const shortId = require('short-id')
const validUrl = require('valid-url')

const client = require('../db/redis')

const baseUrl = 'http://localhost:3000/short/'

const {handleSuccess, handleError} = require("../utils/requestHandlers");

router.get('/:shortUrl', (req, res) => {
    const {shortUrl} = req.params
    client.hmget(shortUrl, 'originalUrl', (err, reply) => {
        if(err) {
            return handleError(res, err, message)

        } else if(!reply[0]) {
            return handleError(res, err, 'Not found')

        }
        const originalUrl = reply[0]
        res.status(301).redirect(originalUrl)
    })
})

router.post('/', (req, res) => {
    const {original_url} = req.body

    if(!validUrl.isUri(original_url)) {
        return handleError(res, err, 'Invalid url!')
    }
    
    const shortID = shortId.generate()
    const shortenedUrl = baseUrl + shortID

    client.hmset(shortID, ['originalUrl', original_url], (err, reply) => {
        if(err) {
            return handleError(res, err)
        }
        handleSuccess(res,
            {
                shortenedUrl,
                urlId: shortID
            },
			'Success'
        )
    })
})

module.exports = {
    router,
    path: '/short'
}
